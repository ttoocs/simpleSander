include <../3d/technicBeams.scad>
use <../3d/alignment.scad>
use <../3d/raft.scad>
use <../3d/up3dBox.scad>


wScale = 1.01;
wlen=200.0;
wwidth=37.5*wScale;
wdepth=17.5*wScale;

echo( wwidth);
echo( wdepth);


Jdepth = 5; //Depth on either side of joint//
            //And top/bottom

module woodChunk() {
    len=wlen;
    difference(){
//        union(){
            cube([wwidth,wdepth,len]);
//        }
        union(){        
            translate([0,-wdepth/2,0]){rotate([0,60,0]){
                cube([wwidth,wdepth*2,len]);
            }}
            translate([wdepth,-wdepth/2,len+wwidth]){rotate([0,120,0]){
                cube([wwidth,wdepth*2,len]);
            }}
        }
    }   
}


module joint(){
    translate([0,0,wwidth/cos(30)]){
    union(){
        translate([0,0,-0.1]){ //Fudge it closer to make it solid.
            woodChunk();
        }
    translate([0,wdepth,-wwidth/cos(30) + 0.1]){
        rotate([0,0,180]){
            rotate([0,-60,0]){
                woodChunk();
           
    }
    }
    }
    }
    }
}
//joint();

module tri(){
    /* //Made it via poly, didn't like it. /shrug.
    polyhedron(points=
    [[0,0,0],[sin(60),cos(60),0],[0,1,0],
     [0,0,1],[sin(60),cos(60),1],[0,1,1]],
    faces=[[0,1,2],[3,4,5],[0,3,4,1],[4,1,2,5],[2,5,3,0]]);
    // */
    scale([2,2,1]){
    difference()
    {
        cube([1,1,1]);
        
        union(){
            translate([0,0,-0.2]){
        rotate([0,0,-60]){
            cube([1,2,1.5]);
        }}
        translate([sin(60),0,-0.2]){
        rotate([0,0,60]){
            
            cube([1,2,1.5]);
            }
        }
        }
    }
    }
}

//tri();

module ctri(){
    translate([-sin(60)/3,-.5,0]){
        tri();
    }
}

//ctri();

module FullJoint(){

// /*
union(){

difference()
    {//Take inv of wood to make the support

    union(){ 
        scale([150,Jdepth*2+wdepth,150]){
        translate([cos(60)*sqrt(3)/3,0,sin(60)*sqrt(3)/3]){
        rotate([-90,120,0]){
            ctri();
        }
        }
        }
    }


    translate([Jdepth*cos(60)*2,Jdepth,Jdepth*sin(60)*2]){
        joint();
    }
}

// */

//Unioned with technic stuffs:
// /*
N=17;
translate([0,-technicWidth+0.12,0])
{
    rotate([0,-60,0])
    {
        union(){
        translate([0,0,-technicHeight*1.5 - technicHeight]){
            technicSolidBeam(N);
        }
        translate([0,0,technicHeight*0.5 + technicHeight]){
            technicSolidBeam(N);
        }
        }
    }
}
// */

}
}
// FullJoint();

// */
module rotJnt(){
translate([0,0,(Jdepth*2 + wdepth)]){
rotate([-90,0,0]){
FullJoint();
}
}
}
 //rotJnt();



VertFudge=4.85; //Done by hand via intersect.

module bottom(){
    union()
    //intersection()
    {
    difference()
        {
    
        rotJnt();
    
        translate([-50,-50,(Jdepth + wdepth)-0.1]){
        cube([500,500,500]);
        }
    }
    //Connection Pins!
    union(){
        
        translate([Jdepth*cos(60),sin(60)*Jdepth,
            (Jdepth + wdepth) + VertFudge]){
        rotate([0,180,-90]){
            alignmentA([130,5,5]);
        }}
        
        translate([Jdepth*cos(60),sin(60)*Jdepth,
            (Jdepth + wdepth) + VertFudge]){
            rotate([0,180,-150]){
            translate([10,0,0]){
                alignmentA([115,5,5]);
            }
        }}
        
        d = Jdepth+wwidth;
        translate([d + Jdepth ,(d+ Jdepth)/tan(30) ,
        (Jdepth + wdepth)+VertFudge] ){
        rotate([0,180,-150])
            {
            
            translate([0,0,0]){
                alignmentA([25,5,5]);
            }
        }}
    
        }
    
    }
    
}

//bottom();

module top(){
   difference(){ //Connection diffs
   difference(){
        rotJnt();
    
        translate([-50,-50,-500 + ((Jdepth + wdepth)+0.1)]){
        cube([500,500,500]);
        }
    }
    
    //Connection Pins!
        union(){
        
        translate([Jdepth*cos(60),sin(60)*Jdepth,
            (Jdepth + wdepth) + VertFudge]){
        rotate([0,180,-90]){
            alignmentB([130,5,5]);
        }}
        
        translate([Jdepth*cos(60),sin(60)*Jdepth,
            (Jdepth + wdepth) + VertFudge]){
            rotate([0,180,-150]){
            translate([10,0,0]){
                alignmentB([115,5,5]);
            }
        }}
        
        d = Jdepth+wwidth;
        translate([d + Jdepth ,(d+ Jdepth)/tan(30) ,
        (Jdepth + wdepth)+VertFudge] ){
        rotate([0,180,-150])
            {
            
            translate([0,0,0]){
                alignmentB([25,5,5]);
            }
        }}
    
        }
    } //end connection diffs
    
    
}
//top();




module tring(D1,D2){
    difference(){
        
        scale([D1,D1,1]){
            ctri();
        }
        translate([0,0,-.5]){
        scale([D2,D2,2]){
            ctri();
        }
        }
    }
}

//tring(1.5,1);
//tri();

//intersection(){
//top();
//bottom();
//}

// /*
intersection()
{


intersection(){
    upPlus2Box();
    union()
    //intersection()
    {
    translate([70,-30,10]){
        rotate([0,0,30]){
            //top();
            bottom();
        }
    }
    //Raft!
    translate([20,10,(Jdepth + wdepth)-VertFudge/3]){
    //raft([100,100,2]);
    }
    }
}

union(){
    rad=90;
    translate([70,50,0]){
    rotate([0,0,-30]){
    scale([1,1,50]){
        tring(rad,rad/2);
    }
    }
    }
}
}
// */
//raft([200,200,2]);

