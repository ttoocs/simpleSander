
dseg segment
  apos vec2
  bpos vec2
  cpos vec2
  pmid vec2
ends

////////////////////////////////////////////////////////////////////////////////
// Compile-time config:
////////////////////////////////////////////////////////////////////////////////
// LENGTHS between motors:
#define AB 55.0
#define AC 55.0
#define BC 55.0

//radius in cm
#define panSize 14.0
// 9 rotations, made 35cm of movement.
// DEGRESS PER CENTIMETER
// #define _movRate valueof(valueof(9*360)/35.0)
//#define movRate -92.571 //Pre-gear
#define movRate 277.71 //3:1 gear ratio, also negates it.


//Inverse cossin-law, takes the neighboring side lengths,
//  the length of the far side
//  Leaves the resulting angle on stack
#define acosLaw(A,B,C) \
  fs_push(2)  fs_push(A)  fs_push(B)  fs_mul  fs_mul \
  \
  fs_push(A)  fs_push(A)  fs_mul \
  fs_push(B)  fs_push(B)  fs_mul \
  \
  fs_push(-1) fs_push(C)  fs_push(C)  fs_mul  fs_mul \
  fs_add  fs_add \
  \
  fs_div \
  fs_acosd \


//Each define/calculate the angle, leaving it on the stack.
#define Aangle acosLaw(AB,AC,BC) \

#define Bangle acosLaw(AB,BC,AC) \

#define Cangle acosLaw(AC,BC,AB) \


// vec2 apos;
#define calc_apos_x  fs_push(0)
#define calc_apos_y  fs_push(0)
// vec2 bpos;
#define calc_bpos_x  fs_push(AB)
#define calc_bpos_y  fs_push(0)

// vec2 cpos; //Uses both, averages. /shrug
//#define cpos_x  (cosd(Aangl)*AC  + cosd(Bangl)*BC )/2
//#define cpos_y  (sind(Aangl)*AC  + sind(Bangl)*BC)/2

#define calc_cpos_x \
  Aangle fs_cosd fs_push(AC) fs_mul \
  Bangle fs_cosd fs_push(BC) fs_mul \
  fs_add fs_push(2)  fs_Rdiv \

#define calc_cpos_y \
  Aangle fs_sind fs_push(AC) fs_mul \
  Bangle fs_sind fs_push(BC) fs_mul \
  fs_add fs_push(2) fs_Rdiv \

// vec2 pmid; //Pan middle
//#define  pmid_x  (apos_x+bpos_x+cpos_x)/3
//#define  pmid_y  (apos_y+bpos_y+cpos_y)/3

#define calc_pmid_x \
  apos_x bpos_x calc_cpos_x fs_add fs_add fs_push(3) fs_Rdiv \

#define calc_pmid_y \
  apos_y bpos_y calc_cpos_y fs_add fs_add fs_push(3) fs_Rdiv \


#define _init_pos_helper1(vec) \
  calc_##vec##_x fs_pop(vec.x) \
  calc_##vec##_y fs_pop(vec.y) \


#define init_pos \
  _init_pos_helper1(apos) \
  _init_pos_helper1(bpos) \
  _init_pos_helper1(cpos) \
  _init_pos_helper1(pmid) \
  pmid_xy \
  v2s_pop(objPos) \

//Use cached values to push to stack.
#define apos_xy v2s_push(apos)
#define apos_x fs_push(apos.x)
#define apos_y fs_push(apos.y)

#define bpos_xy v2s_push(bpos)
#define bpos_x fs_push(bpos.x)
#define bpos_y fs_push(bpos.y)

#define cpos_xy v2s_push(cpos)
#define cpos_x fs_push(cpos.x)
#define cpos_y fs_push(cpos.y)

#define pmid_xy v2s_push(pmid)
#define pmid_x fs_push(pmid.x)
#define pmid_y fs_push(pmid.y)


////////////////////////////////////////////////////////////////////////////////
//NOTES//
////////////////////////////////////////////////////////////////////////////////
//Rate of movement: \
  //9 rotations = 35cm \
  //~3.89 cm per rotation. \
  //Note, it will vary with depth.. but this is good enough I suppose. \

//Q: How to get the current position? \
  //Start it at somepoint, and just keep track \
  //Have a way to re-set it \

//Q: How to move it about: \
  //Ideally moving it about so everywhere is equally sanded in all directions \
  //Move about in circles along a slightly larger circle? \
  //Abstract it to X/Y? \

//Q: Abs to X/Y \
  // Just use baricentric? \
////////////////////////////////////////////////////////////////////////////////

//Takes a scaler (compenent of barcentric), returns the vec.
#define weightA(Vec,sc) \
  fmul(apos.x,sc,Vec.x) \
  fmul(apos.y,sc,Vec.y) \

#define weightB(Vec,sc) \
  fmul(bpos.x,sc,Vec.x) \
  fmul(bpos.y,sc,Vec.y) \

#define weightC(Vec,sc) \
  fmul(cpos.x,sc,Vec.x) \
  fmul(cpos.y,sc,Vec.y) \

//Calculates the weight using floats of A/B/C position

#define getPosABC(A,B,C,R) \
  weightA(R,A) \
  v2s_push(R) \
  weightB(R,B) \
  v2s_push(R) \
  weightC(R,C) \
  v2s_push(R) \
  v2s_add \
  v2s_add \
  v2s_pop(R)\

