
dseg segment
  STACK_FLOAT float[]
  STACK_FLOAT_IDX byte
  SF_T1 float
  SF_T2 float

  vec2 struct
    x float
    y float
  vec2 ends

  SF_V1 vec2
  SF_V2 vec2

ends

#define InitStack \
  arrinit STACK_FLOAT, 0, 128

//Float stack ops:

#define fs_read(addr,R) \
  index R, STACK_FLOAT, addr \

#define fs_read_off(off,R) \
  sub R, STACK_FLOAT_IDX, off \
  fs_read(R, R) \

#define fs_write(addr,X) \
  replace STACK_FLOAT, STACK_FLOAT, addr, X \

#define fs_push(X) \
  add STACK_FLOAT_IDX, STACK_FLOAT_IDX, 1 \
  fs_write(STACK_FLOAT_IDX,X) \

#define fs_pop(R) \
  fs_read(STACK_FLOAT_IDX,R) \
  sub STACK_FLOAT_IDX, STACK_FLOAT_IDX, 1 \

  //Duplicates an element
#define fs_dup \
  fs_read(STACK_FLOAT_IDX,SF_T1) \
  fs_push(SF_T1) \

  //Duplicates an element higher in the stack
  //Note: 0 is duplicating top of stack
#define fs_dup_n(n) \
  sub   SF_T1, STACK_FLOAT_IDX, n \
  fs_read(SF_T1,SF_T1) \
  fs_push(SF_T1) \

#define fs_del_n(n) \
  sub STACK_FLOAT_IDX, STACK_FLOAT_IDX, n \

#define fs_del fs_del_n(1)

#define fs_colapse \
  fs_pop(SF_T1) \  fs_del \ fs_push(SF_T1) \

//Overwrite offset with value
#define fs_overwrite(offset,value) \
  sub   SF_T1, STACK_FLOAT_IDX, offset \
  replace STACK_FLOAT, STACK_FLOAT, SF_T1, value \

//Overwrite offset from offset2
#define fs_overwrite_fs(offset,offset2) \
  sub   SF_T1, STACK_FLOAT_IDX, offset2 \
  fs_read(SF_T1,SF_T2) \
  sub   SF_T1, STACK_FLOAT_IDX, offset \
  fs_write(SF_T1,SF_T2) \

// Stack Op helpers
#define fs_1op(op) \
  fs_pop(SF_T1) \
  op SF_T1, SF_T1 \
  fs_push(SF_T1) \

#define fs_2op(op) \
  fs_pop(SF_T1) \
  fs_pop(SF_T2) \
  op SF_T1, SF_T1, SF_T2 \
  fs_push(SF_T1) \

//Reversed
#define fs_2Rop(op) \
  fs_pop(SF_T2) \
  fs_pop(SF_T1) \
  op SF_T1, SF_T1, SF_T2 \
  fs_push(SF_T1) \

 //Float stack operations
#define fs_asind fs_1op(asind)
#define fs_sind fs_1op(sind)
#define fs_acosd fs_1op(acosd)
#define fs_cosd fs_1op(cosd)
#define fs_atand fs_1op(atand)
#define fs_tand fs_1op(tand)
#define fs_abs  fs_1op(abs)
#define fs_sqrt fs_1op(sqrt)
#define fs_sign fs_1op(sign)
#define fs_neg fs_1op(neg)

#define fs_add fs_2op(add)
#define fs_sub fs_2op(sub)
#define fs_mul fs_2op(mul)
#define fs_div fs_2op(div)
#define fs_Rdiv fs_2Rop(div)
#define fs_Rsub fs_2Rop(sub)

#define fs_min \
  call __fs_min

subroutine __fs_min
  fs_pop(SF_T1)
  fs_pop(SF_T2)
  brcmp GTEQ, __fs_min_gt, SF_T1, SF_T2
  fs_push(SF_T1)
  return
  __fs_min_gt:
    fs_push(SF_T2)
  return
ends

#define fs_max \
  call __fs_max

subroutine __fs_max
  fs_pop(SF_T1)
  fs_pop(SF_T2)
  brcmp GTEQ, __fs_max_gt, SF_T1, SF_T2
  fs_push(SF_T2)
  return
  __fs_max_gt:
    fs_push(SF_T1)
  return
ends

//Safe float ops (via stack)

#define fop1(op,X,R) \
  fs_push(X) \
  op \
  fs_pop(R) \

#define fop2(op,X,Y,R) \
  fs_push(X) \
  fs_push(Y) \
  op \
  fs_pop(R) \

#define fadd(X,Y,R) fop2(fs_add,X,Y,R)
#define fsub(X,Y,R) fop2(fs_sub,X,Y,R)
#define fmul(X,Y,R) fop2(fs_mul,X,Y,R)
#define fdiv(X,Y,R) fop2(fs_div,X,Y,R)


//Vectors Stack Ops (same stack as floats)

#define v2s_push(X)\
  fs_push(X.x) \
  fs_push(X.y) \

#define v2s_pop(R) \
  fs_pop(R.y) \
  fs_pop(R.x) \

#define v2s_dup \
  v2s_pop(SF_V1) \
  v2s_push(SF_V1) \
  v2s_push(SF_V1) \

//vec2, 1op, component wise
#define _v2_op1_comp(op) \
  v2s_pop(SF_V1) \
  v2s_pop(SF_V2) \
  op SF_V1.x, SF_V1.x \
  op SF_V1.y, SF_V1.y \
  v2s_push(SF_V1) \

//vec2, 2op, component wise
#define _v2_op2_comp(op) \
  v2s_pop(SF_V1) \
  v2s_pop(SF_V2) \
  op SF_V1.x, SF_V1.x, SF_V2.x \
  op SF_V1.y, SF_V1.y, SF_V2.y \
  v2s_push(SF_V1) \

//Vec2, 2op, compenentwise, reversed
#define _v2_op2_comp_r(op) \
  v2s_pop(SF_V2) \
  v2s_pop(SF_V1) \
  op SF_V1.x, SF_V1.x, SF_V2.x \
  op SF_V1.y, SF_V1.y, SF_V2.y \
  v2s_push(SF_V1) \

#define v2s_mul _v2_op2_comp(mul)
#define v2s_div _v2_op2_comp(div)

#define v2s_add _v2_op2_comp(add)

#define v2s_sub _v2_op2_comp(sub)
#define v2s_Rdiv _v2_op2_comp_r(div)
#define v2s_Rsub _v2_op2_comp_r(sub)
#define v2s_abs _v2_op1_comp(abs)

//Calcs length
#define v2s_length \
  v2s_sub \
  v2s_dup \
  v2s_mul \
  fs_add \
  fs_sqrt \


//Safe vector ops (via stack)
#define v2_add(X,Y,R) \
  v2s_push(X)\
  v2s_push(Y)\
  v2s_add\
  v2s_pop(R)\

#define v2_mul(X,Y,R) \
  v2s_push(X)\
  v2s_push(Y)\
  v2s_mul\
  v2s_pop(R)\


//Assumes tdinf is a TDrawText element
#define test_float_stack \
  mov tdinf.Location.X, 32 \
  mov tdinf.Location.Y, 32 \
  fs_push(0.25) \
  fs_push(10) \
  fs_mul \
  numtostr tdinf.Text, SF_T1 \
  syscall DrawText, tdinf \

#define _print_helper(X,Y,R,opt) \
  mov tdinf.Location.X, X \
  mov tdinf.Location.Y, Y \
  mov tdinf.Options, opt \
  numtostr tdinf.Text,  R \
  syscall DrawText, tdinf \

//Prints stack, with bottom element at bottom
//5th element near top.
#define print_stack_off(n) \
  mov SF_T2, n \
  fs_read(SF_T2,SF_T1) _print_helper(2,0,SF_T1,1) \
  _print_helper(60,0,SF_T2,0) \
  sub SF_T2, SF_T2, 1 \
  \
  fs_read(SF_T2,SF_T1) _print_helper(2,10,SF_T1,0) \
  _print_helper(60,10,SF_T2,0) \
  sub SF_T2, SF_T2, 1 \
  \
  fs_read(SF_T2,SF_T1) _print_helper(2,20,SF_T1,0) \
  _print_helper(60,20,SF_T2,0) \
  sub SF_T2, SF_T2, 1 \
  \
  fs_read(SF_T2,SF_T1) _print_helper(2,30,SF_T1,0) \
  _print_helper(60,30,SF_T2,0) \
  sub SF_T2, SF_T2, 1 \
  \
  fs_read(SF_T2,SF_T1) _print_helper(2,40,SF_T1,0) \
  _print_helper(60,40,SF_T2,0) \
  sub SF_T2, SF_T2, 1 \
  \
  fs_read(SF_T2,SF_T1) _print_helper(2,50,SF_T1,0) \
  _print_helper(60,50,SF_T2,0) \
  sub SF_T2, SF_T2, 1 \
  \

#define print_stack \
  print_stack_off(STACK_FLOAT_IDX)


#define print \
 print_stack \
 wait 750 \



#define test_max_min(TESTA,TESTB) \
  fs_push(TESTA) \
  fs_push(TESTB) \
  fs_push(TESTA) \
  fs_push(TESTB) \
  fs_min \
  print \
  fs_del_n(3) \
  \
  fs_push(TESTA) \
  fs_push(TESTB) \
  fs_push(TESTA) \
  fs_push(TESTB) \
  fs_max \
  print \
  fs_del_n(3) \

