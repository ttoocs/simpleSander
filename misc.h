
#define MISC_INCLUDED

dseg segment
//Data segments
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
//misc tmp vals
////////////////////////////////////////
tval struct
  stmp1 sbyte
  stmp2 sbyte
  stmp3 sbyte
  stmp4 sbyte
  swrd1 sword
tval ends

  tsw sword
  tmp1 tval
  tmp2 tval
  tmp3 tval
  tmp4 tval

////////////////////////////////////////
//Float temp vals
////////////////////////////////////////

  tv vec2
  v1 vec2
  v2 vec2
  v3 vec2
  v4 vec2

fval struct
  ftmp1 float
  ftmp2 float
  ftmp3 float
  ftmp4 float
fval ends

  ftmp1 fval
  ftmp2 fval
  ftmp3 fval
  ftmp4 fval

ends

#define Stop  Coast(OUT_A)\
              Coast(OUT_B)\
              Coast(OUT_C)

#define getDeg(Tbyte,MOT)\
  getout Tbyte, MOT, TachoCount

#define testTime 1000

//Simple wrapper to test motors tension
#define test(T4,MOT) \
  tester(T4,MOT,10,1)

//Test a motors tension by pulling it at Pwr, and optionally resetting it.
#define tester(T4,MOT,Pwr,reSet) \
  getDeg(T4.stmp1,MOT) \
  OnFwd(MOT,Pwr) \
  sleep(testTime) \
  Coast(MOT)  \
  getDeg(T4.stmp2,MOT) \
  sub T4.stmp1, T4.stmp2, T4.stmp1 \
  numtostr tdinf.Text, T4.stmp1 \
  mov tdinf.Location.X 32 \
  mov tdinf.Location.Y 32 \
  syscall DrawText, tdinf \
  sub T4.stmp2, zero, T4.stmp2 \
  GetOnBrickProgramPointer(T4.stmp3) \
  mul T4.stmp3, testTime, reSet \
  RotateMotor(MOT,50,T4.stmp2) \
  sleep(testTime) \
  Coast(MOT)

//Pull a motor X, and reverse the other ones.
//Doesn't work, OUT_A is zero, screwing up everything.
/*
#define PullX(T,MOT,FPWR,RPWR,DELAY) \
  mov T.stmp1, RPWR \
  cmp EQ, T.stmp2, MOT, OUT_A \
  cmp EQ, T.stmp3, MOT, OUT_B \
  cmp EQ, T.stmp4, MOT, OUT_C \
  mul T.stmp2, T.stmp2, OUT_A \
  mul T.stmp3, T.stmp3, OUT_B \
  mul T.stmp4, T.stmp4, OUT_C \
  OnFwd(T.stmp2,FPWR) \
  OnFwd(T.stmp3,FPWR) \
  OnFwd(T.stmp4,FPWR) \
  cmp NEQ, T.stmp2, MOT, OUT_A \
  cmp NEQ, T.stmp3, MOT, OUT_B \
  cmp NEQ, T.stmp4, MOT, OUT_C \
  mul T.stmp2, T.stmp2, OUT_A \
  mul T.stmp3, T.stmp3, OUT_B \
  mul T.stmp4, T.stmp4, OUT_C \
  OnRev(T.stmp2,RPWR) \
  OnRev(T.stmp3,RPWR) \
  OnRev(T.stmp4,RPWR) \
  wait DELAY\
  Stop
*/

#define PullX(T,MOT,FPWR,RPWR,DELAY) \
  mov tmp1.stmp1, MOT \
  mov tmp1.stmp2, FPWR \
  mov tmp1.stmp3, RPWR \
  mov tmp1.swrd1, DELAY \
  call S_PullX \

subroutine S_PullX

  brcmp EQ, S_PX_A, tmp1.stmp1, OUT_A
  brcmp EQ, S_PX_B, tmp1.stmp1, OUT_B
  brcmp EQ, S_PX_C, tmp1.stmp1, OUT_C
  return

  S_PX_A:
    OnFwd(OUT_A,tmp1.stmp2)
    OnRev(OUT_B,tmp1.stmp3)
    OnRev(OUT_C,tmp1.stmp3)
    jmp S_PX_END

  S_PX_B:
    OnRev(OUT_A,tmp1.stmp3)
    OnFwd(OUT_B,tmp1.stmp2)
    OnRev(OUT_C,tmp1.stmp3)
    jmp S_PX_END

  S_PX_C:
    OnRev(OUT_A,tmp1.stmp3)
    OnRev(OUT_B,tmp1.stmp3)
    OnFwd(OUT_C,tmp1.stmp2)
    jmp S_PX_END

  S_PX_END:
    waitv tmp1.swrd1 //Doesn't seem to actually be waiting??
    Stop
    return
ends


#define PullA(X,Y) \
  PullX(tmp1,OUT_A,X,20,Y)
/*
  mov stmp, 20 \
  OnFwd(OUT_A,X)\
  OnRev(OUT_B,stmp)\
  OnRev(OUT_C,stmp)\
  wait Y\
  Stop
*/

#define PullB(X,Y)\
  PullX(tmp1,OUT_B,X,20,Y)
/*
  mov stmp, 20\
  OnRev(OUT_A,stmp)\
  OnFwd(OUT_B,X)\
  OnRev(OUT_C,stmp)\
  wait Y\
  Stop
*/

#define PullC(X,Y)\
  PullX(tmp1,OUT_C,X,20,Y)
/*
  mov stmp, 20\
  OnRev(OUT_A,stmp)\
  OnRev(OUT_B,stmp)\
  OnFwd(OUT_C,X)\
  wait Y\
  Stop
*/

#define Pull(PWR,DELAY)   \
  OnFwd(OUT_A,PWR)\
  OnFwd(OUT_B,PWR)\
  OnFwd(OUT_C,PWR)\
  wait DELAY\
  Stop

#define FPXTa 5000
#define FPXTb 10000


#define FPA1 \
  PullA(100,FPXTa)\
//  Pull(25,FPXTb)

#define FPB1 \
  PullB(100,FPXTa)\
//  Pull(25,FPXTb)

#define FPC1 \
  PullC(100,FPXTa)\
//  Pull(25,FPXTb)

#define FPA2 \
  FPA1\
  FPA1\

#define FPA4 \
  FPA2\
  FPA2\

#define FPA8 \
  FPA4\
  FPA4\

#define FPB2 \
  FPB1\
  FPB1

#define FPB4 \
  FPB2\
  FPB2

#define FPB8 \
  FPB4\
  FPB4

#define FPC2 \
  FPC1\
  FPC1

#define FPC4 \
  FPC2\
  FPC2

#define FPC8 \
  FPC4\
  FPC4

#define FIXTIME 10000
#define MOVTIME 5000

