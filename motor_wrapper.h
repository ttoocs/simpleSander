//Better motor wrapper, in that it doesn't move only one motor at a time.

dseg segment
  _btrmotCnt byte 0
  _btrmotCntDone byte 0
  motCmdPwr byte
  motCmdDeg slong
ends

#define motXMov(lbl,MOT) \
thread lbl \
  add _btrmotCnt, _btrmotCnt, 1 \
  RotateMotor(MOT,motCmdPwr,motCmdDeg) \
  add _btrmotCntDone, _btrmotCntDone, 1 \
  exit \
endt \

motXMov(motAMov,OUT_A)
motXMov(motBMov,OUT_B)
motXMov(motCMov,OUT_C)

#define BetterRotMot(THREAD,PWR,DEG) \
  mov motCmdPwr, PWR \
  mov motCmdDeg, DEG \
  start THREAD \
  wait 100 \

#define BetterRotMot_Wait \
  call __BetterRotatMot_Wait \

subroutine __BetterRotatMot_Wait
  __betMorWaitStart:
  wait 10
//  fs_push(_btrmotCnt) fs_push(_btrmotCntDone) print fs_del_n(2) 
  brcmp NEQ, __betMorWaitStart, _btrmotCnt, _btrmotCntDone
  mov _btrmotCnt, 0
  mov _btrmotCntDone, 0
  return
ends

