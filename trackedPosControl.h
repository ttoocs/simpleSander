#define TRACKED_POSITIONAL_CONTROL_INCLUDED

//A system of control based on keeping track of the position of the object:

//Calculates the rate a moter will move the object at CurPos (as a vector, by default at 1 abstract unit)
// abs(C.x-t.x)
// abs(C.y-t.y)
//
#define calcPullRate(CurPos,TargetPos) \
  fs_push(CurPos.x) fs_push(TargetPos.x) fs_sub             \
  fs_push(CurPos.y) fs_push(TargetPos.y) fs_sub             \
  fs_dup_n(1) fs_dup fs_mul /* x^2 */                       \
  fs_dup_n(1) fs_dup fs_mul /* y^2 */                       \
  fs_add                      /* Gets length squared */     \
  fs_sqrt                                                   \
    /* STACK: x_length, y_length, length */                 \
  fs_dup                                                    \
  fs_dup_n(3) fs_div       /*grab x and div */              \
    /* STACK: x_length, y_length, length^2, x/length^2 */   \
  fs_overwrite_fs(3,0)    /* Overwrite the old X */         \
  fs_del                  /* Get rid of X on top of stack*/ \
  fs_Rdiv                 /* Div y/length */                \
    /* STACK: xlen/len^2, ylen/len^2 */                     \
  v2s_push(CurPos) v2s_push(TargetPos) v2s_length           \
  fs_dup \
  v2s_mul \

//Takes the releaseing position, current position of object, and the targeted position of object.
//Returns the rate to feed (by default at 1 abstract unit of movement)
#define calcReleaseRate(ReleasePos,CurPos,TargetPos)\
  v2s_push(CurPos) calcPullRate(CurPos,TargetPos)  v2s_add   /* gets new pos of obj */ \
  v2s_push(ReleasePos) v2s_length \
  v2s_push(CurPos) v2s_push(ReleasePos) v2s_length \
  fs_sub \

//Simple
#define fs_toMot(MOT) \
  fs_pop(SF_T1) \
  OnFwd(MOT,SF_T1) \

//Rotational
#define fs_toMotA(MOT) \
  fs_push(movRate) \
  fs_mul \
  fs_pop(SF_T1) \
  RotateMotor(MOT,100,SF_T1) \

#define moveTo(CurPos,Target) \
  calcReleaseRate(apos,CurPos,Target) \
  calcReleaseRate(bpos,CurPos,Target) \
  calcReleaseRate(cpos,CurPos,Target) \
  fs_toMotA(OUT_C) \
  fs_toMotA(OUT_B) \
  fs_toMotA(OUT_A) \

dseg segment
  tmp_sword sword
  gp_lenA float
  gp_lenB float
  gp_lenC float
  gp_theta1 float
  gp_theta2 float
  gp_pos1 vec2
  gp_pos2 vec2
  gp_pos3 vec2
  gp_pos4 vec2
  gp_pos5 vec2
  gp_pos6 vec2
  gp_i    byte
  gp_idx  byte
  gp_min  float
dseg ends

#define axsis_aligned_cos_2(theta1,r1,theta2,r2)\
  fs_push(theta1)  fs_cosd fs_push(r1) fs_mul \
  fs_push(theta2)  fs_cosd fs_push(r2) fs_mul \
  fs_add  fs_push(2)  fs_Rdiv\

#define axsis_aligned_sin_2(theta1,r1,theta2,r2)\
  fs_push(theta1)  fs_sind fs_push(r1) fs_mul \
  fs_push(theta2)  fs_sind fs_push(r2) fs_mul \
  fs_add  fs_push(2)  fs_Rdiv\

#define _getPos_helper_intersect(L1,L2,L3,V1,V2)\
  acosLaw(L1,L2,L3) \
  fs_pop(gp_theta1) \
  acosLaw(L1,L3,L2) \
  fs_pop(gp_theta2) \
  axsis_aligned_cos_2(gp_theta1,L2,gp_theta2,L3) \
  axsis_aligned_sin_2(gp_theta1,L2,gp_theta2,L3) \
  v2s_pop(V1) \
  neg gp_theta1, gp_theta1 \
  neg gp_theta2, gp_theta2 \
  axsis_aligned_cos_2(gp_theta1,L2,gp_theta2,L3) \
  axsis_aligned_sin_2(gp_theta1,L2,gp_theta2,L3) \
  v2s_pop(V2) \

//Using the method from Paul bourke
#define _getPos_helper_intersect2(L1,P1,L2,P2,V1,V2) \
  v2s_push(P1) v2s_push(P2) v2s_length \
  fs_read_off(0,gp_min) \
  \
  fs_dup fs_dup fs_mul \
  fs_push(L1) fs_dup fs_mul fs_push(L2) fs_dup fs_mul \
  fs_Rsub \
  \
  fs_add \
  fs_div fs_push(2) fs_Rdiv \
  /* Stack now has a on it */ \
  fs_read_off(0,gp_theta1) \
  fs_dup fs_mul \
  fs_push(L1) fs_dup fs_mul \
  fs_sub \
  /* fs_push(0) fs_max */ \
  fs_sqrt \
  /* Stack now has h on it */ \
  fs_pop(gp_theta2) \
  v2s_push(P1)  \
  fs_push(gp_theta1) fs_dup /* a as a vec */ \
  v2s_push(P2) v2s_push(P1) v2s_Rsub \
  v2s_mul \
  fs_push(gp_min) fs_dup v2s_Rdiv\
  v2s_add \
  /* Stack has P2 on it */ \
  v2s_dup \
  _getPos_helper_Paulbourke_tovec(P1,P2,gp_theta2,gp_min) \
  v2s_pop(V1) \
  neg gp_theta2, gp_theta2 \
  _getPos_helper_Paulbourke_tovec(P1,P2,gp_theta2,gp_min) \
  v2s_pop(V2) \

//Assumes P2 is on stack, wants h, and two pos
#define _getPos_helper_Paulbourke_tovec(P1,P2,h,d)\
  fs_dup_n(1) fs_push(h) \
  fs_push(P2.y) fs_push(P1.y) fs_Rsub \
  fs_mul fs_push(d) fs_Rdiv \
  fs_add \
  fs_overwrite_fs(2,0) fs_del \
  \
  fs_push(h) fs_neg \
  fs_push(P2.x) fs_push(P1.x) fs_Rsub \
  fs_mul fs_push(d) fs_Rdiv \
  fs_add \

//Defines a metric to sort by
#define _getPos_helper_length_3p(V1,V2,V3) \
  v2s_push(V1) v2s_push(V2) v2s_length \
  v2s_push(V2) v2s_push(V3) v2s_length \
  v2s_push(V1) v2s_push(V3) v2s_length \
  fs_add fs_add \

#define _getPos_helper_length_2p(V1,V2) \
  v2s_push(V1) v2s_push(V2) v2s_length \


subroutine myGetPos
  //Read in degress (pushing to stack as we do so)
  getTacho(gp_lenA,OUT_A)
  getTacho(gp_lenB,OUT_B)
  getTacho(gp_lenC,OUT_C)
  //Convert to length via movRate
  div gp_lenA, gp_lenA, movRate
  div gp_lenB, gp_lenB, movRate
  div gp_lenC, gp_lenC, movRate
  //Add pmid-offset-len to each.
  v2s_push(apos) v2s_push(pmid) v2s_length fs_pop(gp_theta1)
  add gp_lenA, gp_lenA, gp_theta1
  v2s_push(bpos) v2s_push(pmid) v2s_length fs_pop(gp_theta1)
  add gp_lenB, gp_lenB, gp_theta1
  v2s_push(cpos) v2s_push(pmid) v2s_length fs_pop(gp_theta1)
  add gp_lenC, gp_lenC, gp_theta1

  //distance between circles: AB/AC/BC
    //Check the distance for solutions..? - should always.. work.. may have some err to it tho

  //Get of intersection between things
/*
   Using Acos-law:
  _getPos_helper_intersect(AB,gp_lenA,gp_lenB,gp_pos1,gp_pos2)
  _getPos_helper_intersect(AC,gp_lenA,gp_lenC,gp_pos3,gp_pos4)
  _getPos_helper_intersect(BC,gp_lenB,gp_lenC,gp_pos5,gp_pos6)
*/
  _getPos_helper_intersect2(gp_lenA,apos,gp_lenB,bpos,gp_pos1,gp_pos2)
  _getPos_helper_intersect2(gp_lenA,apos,gp_lenC,cpos,gp_pos3,gp_pos4)
  _getPos_helper_intersect2(gp_lenB,bpos,gp_lenC,cpos,gp_pos5,gp_pos6)


  //Assume array of things to sort on the stack
  #define _gp_sort(lbl,maxitr)\
    mov gp_min, 10000 \
    mov gp_idx, 42 \
    mov gp_i, -1 \
    _gp_sort_start_##lbl: \
      add gp_i, gp_i, 1 \
      brcmp EQ, _gp_sort_end_##lbl, gp_i maxitr \
      /*read next len */ \
      fs_read_off(gp_i,gp_theta1) \
      \
      /*Test for inf/nan */ \
      add gp_theta2, gp_theta1, 100 \
      brcmp EQ, _gp_sort_start_##lbl, gp_theta2, gp_theta1 \
      \
      brcmp GTEQ, _gp_sort_start_##lbl, gp_theta1, gp_min \
      /* else it is less-than: */ \
        /* fs_push(gp_min) fs_push(gp_idx) fs_push(gp_theta1) fs_push(gp_i) print print fs_del_n(4) */ \
        mov gp_idx, gp_i \
        mov gp_min, gp_theta1 \
        jmp _gp_sort_start_##lbl \
    _gp_sort_end_##lbl: \


  //Use 3position intersect: (else it uses two)
//  #define getpos3P

  #ifdef getpos3P

  //Find least-error with respect to eachother 3-points
  _getPos_helper_length_3p(gp_pos2,gp_pos4,gp_pos6)  // 2,2,2 // 7
  _getPos_helper_length_3p(gp_pos2,gp_pos4,gp_pos5)  // 2,2,1 // 6
  _getPos_helper_length_3p(gp_pos2,gp_pos3,gp_pos6)  // 2,1,2 // 5
  _getPos_helper_length_3p(gp_pos2,gp_pos3,gp_pos5)  // 2,1,1 // 4
  _getPos_helper_length_3p(gp_pos1,gp_pos4,gp_pos6)  // 1,2,2 // 3
  _getPos_helper_length_3p(gp_pos1,gp_pos4,gp_pos5)  // 1,2,1 // 2
  _getPos_helper_length_3p(gp_pos1,gp_pos3,gp_pos6)  // 1,1,2 // 1
  _getPos_helper_length_3p(gp_pos1,gp_pos3,gp_pos5)  // 1,1,1 // 0
    //metrics on stack.. sort.
    _gp_sort(3p,8)

  fs_del_n(8) //Get rid of pushed array to stack.

  //Get the positions...
  brcmp EQ, _gp_0, gp_idx, 0
  brcmp EQ, _gp_1, gp_idx, 1
  brcmp EQ, _gp_2, gp_idx, 2
  brcmp EQ, _gp_3, gp_idx, 3
  brcmp EQ, _gp_4, gp_idx, 4
  brcmp EQ, _gp_5, gp_idx, 5
  brcmp EQ, _gp_6, gp_idx, 6
  brcmp EQ, _gp_7, gp_idx, 7
  brcmp EQ, _gp_42, gp_idx, 42  //No good values


  #define select_3p(V1,V2,V3)\
    mov gp_pos1, V1 \
    mov gp_pos2, V2 \
    mov gp_pos3, V3 \
    jmp _gp_mov_end \

  _gp_0:
    select_3p(gp_pos1,gp_pos3,gp_pos5)
  _gp_1:
    select_3p(gp_pos1,gp_pos3,gp_pos6)
  _gp_2:
    select_3p(gp_pos1,gp_pos4,gp_pos5)
  _gp_3:
    select_3p(gp_pos1,gp_pos4,gp_pos6)
  _gp_4:
    select_3p(gp_pos2,gp_pos3,gp_pos5)
  _gp_5:
    select_3p(gp_pos2,gp_pos3,gp_pos6)
  _gp_6:
    select_3p(gp_pos2,gp_pos4,gp_pos5)
  _gp_7:
    select_3p(gp_pos2,gp_pos4,gp_pos6)

  _gp_mov_end:
//    fs_push(gp_idx) fs_push(gp_min) print print fs_del_n(2)
//    v2s_push(gp_pos1) v2s_push(gp_pos2) v2s_push(gp_pos3) print print fs_del_n(6)
  //Take the average of the 3.
    v2s_push(gp_pos1) v2s_push(gp_pos2)  v2s_push(gp_pos3)
    v2s_add     v2s_add
    fs_push(0.3333333)  fs_dup
    v2s_mul
  // */

  #else
    //2pos intersect.
    _getPos_helper_length_2p(gp_pos1,gp_pos3) // 1,1,n // 11
    _getPos_helper_length_2p(gp_pos1,gp_pos4) // 1,2,n // 10
    _getPos_helper_length_2p(gp_pos1,gp_pos5) // 1,n,1 // 9
    _getPos_helper_length_2p(gp_pos1,gp_pos6) // 1,n,2 // 8

    _getPos_helper_length_2p(gp_pos2,gp_pos3) // 2,1,n // 7
    _getPos_helper_length_2p(gp_pos2,gp_pos4) // 2,2,n // 6
    _getPos_helper_length_2p(gp_pos2,gp_pos5) // 2,n,1 // 5
    _getPos_helper_length_2p(gp_pos2,gp_pos6) // 2,n,2 // 4

    _getPos_helper_length_2p(gp_pos3,gp_pos5) // n,1,1 // 3
    _getPos_helper_length_2p(gp_pos3,gp_pos6) // n,1,2 // 2
    _getPos_helper_length_2p(gp_pos4,gp_pos5) // n,2,1 // 1
    _getPos_helper_length_2p(gp_pos4,gp_pos6) // n,2,2 // 0

    _gp_sort(2p,12)
    fs_del_n(12)

  brcmp EQ, _gp_p2_0, gp_idx, 0
  brcmp EQ, _gp_p2_1, gp_idx, 1
  brcmp EQ, _gp_p2_2, gp_idx, 2
  brcmp EQ, _gp_p2_3, gp_idx, 3
  brcmp EQ, _gp_p2_4, gp_idx, 4
  brcmp EQ, _gp_p2_5, gp_idx, 5
  brcmp EQ, _gp_p2_6, gp_idx, 6
  brcmp EQ, _gp_p2_7, gp_idx, 7
  brcmp EQ, _gp_p2_8, gp_idx, 8
  brcmp EQ, _gp_p2_9, gp_idx, 9
  brcmp EQ, _gp_p2_10, gp_idx, 10
  brcmp EQ, _gp_p2_11, gp_idx, 11
  brcmp EQ, _gp_42, gp_idx, 42  //No good values

  #define select_2p(lbl,V1,V2)\
    lbl: \
    mov gp_pos1, V1 \
    mov gp_pos2, V2 \
    jmp _gp_mov_end_2p \

    select_2p( _gp_p2_11 , gp_pos1, gp_pos3)
    select_2p( _gp_p2_10 , gp_pos1, gp_pos4)
    select_2p( _gp_p2_9 , gp_pos1, gp_pos5)
    select_2p( _gp_p2_8 , gp_pos1, gp_pos6)
    select_2p( _gp_p2_7 , gp_pos2, gp_pos3)
    select_2p( _gp_p2_6 , gp_pos2, gp_pos4)
    select_2p( _gp_p2_5 , gp_pos2, gp_pos5)
    select_2p( _gp_p2_4 , gp_pos2, gp_pos6)
    select_2p( _gp_p2_3 , gp_pos3, gp_pos5)
    select_2p( _gp_p2_2 , gp_pos3, gp_pos6)
    select_2p( _gp_p2_1 , gp_pos4, gp_pos5)
    select_2p( _gp_p2_0 , gp_pos4, gp_pos6)

    _gp_mov_end_2p:
    v2s_push(gp_pos1) v2s_push(gp_pos2) v2s_add
    fs_push(.5) fs_dup v2s_mul

  #endif

  return

  //When sorting gives _no_ good values:
  _gp_42:
//    v2s_push(objPos) //Say we didn't move..?
//      v2s_push(v1) //Saying we don't move breaks around 180.
      v2s_push(pmid)
      fs_read(0,gp_theta1) //Inc unused 0-value, for showing #errors
      add gp_theta1, gp_theta1, 1
      fs_write(0,gp_theta1)
    return
ends

